<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'All actions';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>


    <div class="row">
        <div class="col-lg-5">
        <?php foreach ($all_actions as $key => $item):?>
            <?= Html::a($item, ['site/check-action', 'action' => $item], ['class' => 'profile-link']) ?><br>
        <?php endforeach; ?>
        </div>
    </div>
</div>