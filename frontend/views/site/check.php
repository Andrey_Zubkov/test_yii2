<?php 

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>


    <div class="row">
        <div class="col-lg-5">
        <?= Html::a('Админ 1', ['site/admin1'], ['class' => 'profile-link']) ?><br>
        <?= Html::a('Админ 2', ['site/admin2'], ['class' => 'profile-link']) ?><br>
        <?= Html::a('Продавец 1', ['site/seller1'], ['class' => 'profile-link']) ?><br>
        <?= Html::a('Продавец 2', ['site/seller2'], ['class' => 'profile-link']) ?><br>
        </div>
    </div>
</div>