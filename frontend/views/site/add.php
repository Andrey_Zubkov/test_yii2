<?php 

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Add Role';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>


    <div class="row">
        <div class="col-lg-5">
        <?php if(isset($mess) && !empty($mess)): ?>
        	<?= $mess; ?>
        <?php endif; ?>
        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
			<?= $form->field($model, 'role')->dropDownList([
					    $roles
					]); ?>
			<?= $form->field($model, 'action')->textInput(['autofocus' => true]) ?>
			<?= Html::submitButton('Add', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
		<?php ActiveForm::end(); ?>
        </div>
    </div>
</div>