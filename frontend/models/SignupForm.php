<?php
namespace frontend\models;
use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $role;
    public $action;
    const SCENARIO_OLD = 'old';
    const SCENARIO_ROLE = 'role';

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_OLD] = ['username', 'password', 'email', 'role'];
        $scenarios[self::SCENARIO_ROLE] = ['role', 'action'];
        return $scenarios;
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim', 'on' => self::SCENARIO_OLD],
            ['username', 'required' , 'on' => self::SCENARIO_OLD],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.', 'on' => self::SCENARIO_OLD],
            ['username', 'string', 'min' => 2, 'max' => 255, 'on' => self::SCENARIO_OLD],

            ['role', 'trim'],

            ['action', 'required', 'on' => self::SCENARIO_ROLE],

            ['email', 'trim', 'on' => self::SCENARIO_OLD],
            ['email', 'required', 'on' => self::SCENARIO_OLD],
            ['email', 'email', 'on' => self::SCENARIO_OLD],
            ['email', 'string', 'max' => 255, 'on' => self::SCENARIO_OLD],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.', 'on' => self::SCENARIO_OLD],

            ['password', 'required', 'on' => self::SCENARIO_OLD],
            ['password', 'string', 'min' => 6, 'on' => self::SCENARIO_OLD],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->role = ($this->role == 1) ? 'admin' : 'seller';
        $user->setPassword($this->password);
        $user->generateAuthKey();
        if($user->save()) {
            $auth = Yii::$app->authManager;
            $role_user = $auth->getRole($user->role);            
            $auth->assign($role_user, $user->id);
            return $user;
        } else {
            return null;
        }
    }

    public function get_all_roles()
    {
         $role = Yii::$app->authManager->roles;
         $roles = array();
         foreach ($role as $key => $value) {
            $roles[$key] = $value->name;
         }
         return $roles;
    }

    public function add_role()
    {
        $auth = Yii::$app->authManager;
        $role_user = $auth->getRole($this->role);
        $createAction = $auth->createPermission($this->action);
        $createAction->description = $this->action;
        $auth->add($createAction);
        if($auth->addChild($role_user, $createAction))
        {
            return true;
        }

        return fasle;  
    }

    public function all_actions()
    {
        $roles = $this->get_all_roles();
        $all_actions = array();
        foreach ($roles as $key => $role) {
            $auth = Yii::$app->authManager;
            $info_role = $auth->getPermissionsByRole($role);
            foreach ($info_role as $key_rol => $value) {
                $all_actions[] = $value->name;
            }
        }
        return $all_actions;
    }
}
