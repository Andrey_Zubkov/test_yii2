<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\User;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\Roles;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'admin1', 'seller1','add', 'all', 'check-action'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout','all','check-action'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['check'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['admin1','admin2','add'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => ['seller1','seller2'],
                        'allow' => true,
                        'roles' => ['seller'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }     

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {   
        $model = new SignupForm(['scenario' => SignupForm::SCENARIO_OLD]);
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionCheck()
    {

        return $this->render('check');
    }

    public function actionAdmin1()
    {
        if(Yii::$app->user->can('admin1'))
        {
            return $this->render('admin1', ['mess' => 'You are Admin']);
        } else {
            return $this->render('admin1', ['mess' => 'You are not Admin']);
        }
    }

    public function actionAdmin2()
    {
        if(Yii::$app->user->can('admin2'))
        {
            return $this->render('admin2', ['mess' => 'You are Admin']);
        } else {
            return $this->render('admin2', ['mess' => 'You are not Admin']);
        }
        
    }

    public function actionSeller1()
    {
        if(Yii::$app->user->can('seller1'))
        {
            return $this->render('seller1', ['mess' => 'You are Seller']);
        } else {
            return $this->render('seller1', ['mess' => 'You are not Seller']);
        }
    }

    public function actionSeller2()
    {
        if(Yii::$app->user->can('seller2'))
        {
            return $this->render('seller2', ['mess' => 'You are Seller']);
        } else {
            return $this->render('seller2', ['mess' => 'You are not Seller']);
        }
    }

    public function actionAdd()
    {   
        $model = new SignupForm(['scenario' => SignupForm::SCENARIO_ROLE]);
        $roles = $model->get_all_roles();
        if($model->load(Yii::$app->request->post()) && $model->validate())
        {
            if($model->add_role())
            {
                return $this->render('add', ['roles' => $roles, 'model' => $model, 'mess' => 'New action added!']);
            }
        } else {
            
            return $this->render('add', ['roles' => $roles, 'model' => $model,]);
        }
        
    }

    public function actionAll()
    {
        $model = new SignupForm(['scenario' => SignupForm::SCENARIO_ROLE]);
        $all_actions = $model->all_actions();
        return $this->render('all',['all_actions' => $all_actions]);
    }

    public function actionCheckAction($action)
    {
        if(Yii::$app->user->can($action))
        {
            return $this->render('checkAction', ['action' => $action]);
        } else {
            Yii::$app->response->redirect('http://localhost/yii2-advanced-rbac/frontend/web/index.php?r=site%2Fall', 403);
        }
    }
}
