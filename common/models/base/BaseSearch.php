<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 007 07.09.17
 * Time: 17:35
 */

namespace common\models\base;

use yii\base\Model;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;
use common\interfaces\SearchDataProvider;

class BaseSearch extends Model implements SearchDataProvider
{
    /** Event before search process */
    CONST EVENT_BEFORE_SEARCH = 'eventBeforeSearch';

    /** Event after search process */
    CONST EVENT_AFTER_SEARCH = 'eventAfterSearch';

    /** Event before build query */
    CONST EVENT_BEFORE_BUILD_QUERY = 'eventBeforeBuildQuery';

    /** Event after build query */
    CONST EVENT_AFTER_BUIL_QUERY = 'eventAfterBuildQuery';

    /** Event before sort */
    CONST EVENT_BEFORE_SORT = 'eventBeforeSort';

    /** Event after sort */
    CONST EVENT_AFTER_SORT = 'eventAfterSort';

    /**
     * Default number of records which will be shown
     *
     * @var int
     */
    public $default_page_size = 10;

    /**
     * Used for construct ActiveDataProvider
     *
     * You should replace this variable in your Search if you have more then one ActiveDataProvider on one page
     *
     * @var string
     */
    public $page_param = 'page';

    /**
     * Used for construct ActiveDataProvider
     *
     * You should replace this variable in your Search if you have more then one ActiveDataProvider on one page
     *
     * @var string
     */
    public $page_size_param = 'page-size';

    /**
     * Sort rules for current ActiveRecord object
     *
     * ```php
     * return [
     *     'defaultOrder' => [
     *         'created_at' => 'DESC',
     *     ],
     *     'attributes' => [
     *         'status',
     *         'created_at',
     *         'sender_info' => [
     *             'label'     => 'Sender info',
     *             'asc'       => ['r.first_name' => SORT_ASC, 'r.last_name' => SORT_ASC],
     *             'desc'      => ['r.first_name' => SORT_DESC, 'r.last_name' => SORT_DESC],
     *             'default'   => SORT_ASC
     *         ],
     *     ]
     * ]
     * ```
     * @return array
     */
    public function getSort()
    {
        // TODO: Implement getSort() method.

        return [];
    }

    /**
     * This method should be rewritten
     * Return everything model which extended from ActiveRecord
     *
     * ```php
     * User::find()->where(['status' => User::STATUS_ACTIVE])
     * ```
     *
     * @return ActiveRecord[]
     */
    public function buildQuery()
    {
        // TODO: Implement buildQuery() method.

        return false;
    }

    /**
     * This method is called at the start search.
     */
    public function afterSearch()
    {
        $this->trigger(BaseSearch::EVENT_AFTER_SEARCH);
    }

    /**
     * This method is called at the end search.
     */
    public function beforeSearch()
    {
        $this->trigger(BaseSearch::EVENT_BEFORE_SEARCH);
    }

    /**
     * This method is called at the start build query.
     */
    public function afterBuildQuery()
    {
        $this->trigger(BaseSearch::EVENT_AFTER_BUIL_QUERY);
    }

    /**
     * This method is called at the end build query.
     */
    public function beforeBuildQuery()
    {
        $this->trigger(BaseSearch::EVENT_BEFORE_BUILD_QUERY);
    }

    /**
     * This method is called at the start sort.
     */
    public function beforeInitSort()
    {
        $this->trigger(BaseSearch::EVENT_BEFORE_SORT);
    }

    /**
     * This method is called at the end sort.
     */
    public function afterInitSort()
    {
        $this->trigger(BaseSearch::EVENT_AFTER_SORT);
    }

    /**
     * Construct ActiveDataProvider object
     * Used $this->page_param for name on GET request
     * Used $this->page_size_param for count records on page
     *
     * @return false|ActiveDataProvider
     */
    public function search()
    {
        // TODO: Implement search() method.
        $this->beforeSearch();

        $this->beforeBuildQuery();
        $query = $this->buildQuery();
        $this->afterBuildQuery();

        $this->beforeInitSort();
        $sort = $this->getSort();
        $this->afterInitSort();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'      => (\Yii::$app->request->get($this->page_size_param) ? \Yii::$app->request->get($this->page_size_param) : $this->default_page_size),
                'pageParam'     => $this->page_param,
            ],
            'sort' => $sort,
        ]);

        $this->afterSearch();

        return $dataProvider;
    }
}