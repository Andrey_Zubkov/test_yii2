<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class NewUser extends ActiveRecord
{
    public $username = NULL;

    public $password = NULL;

    public static function tableName()
    {
        return '{{%new_users}}';
    }

    public function rules()
    {
        return [
            // name, email, subject и body атрибуты обязательны
            [['username', 'password'], 'required'],
        ];
    }

    public function insert_new_user(array $data)
    {
        $data = $this;
        $data->username = 'fff';
        $data->password = 'wwww';
        $data->save();
        return 1;
        // var_dump($new_user->save($data['NewUser']));
        // if($new_user->save($data['NewUser'])) {
        //     return true;
        // }
    }

}
