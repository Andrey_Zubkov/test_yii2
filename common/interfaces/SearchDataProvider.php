<?php

namespace common\interfaces;

use yii\data\ActiveDataProvider;

interface SearchDataProvider {

    /**
     * Used for construct ActiveDataProvider's sort rules
     *
     * @return array
     */
    public function getSort();

    /**
     * Used for construct ActiveDataProvider's qeury
     * This method automation load current Model's data
     *
     * @return false|\yii\db\ActiveRecord[]
     */
    public function buildQuery();

    /**
     * Return object ActiveDataProvider with $this->getSort() and $this->buildQuery()
     *
     * @return ActiveDataProvider
     */
    public function search();
}