<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'calculator' => [
            'class' => 'backend\components\Calculator',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
  		],
    ],
];
