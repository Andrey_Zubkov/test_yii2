<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        // добавляем разрешение "createPost"
        $createAdmin1 = $auth->createPermission('admin1');
        $createAdmin1->description = 'Admin1';
        $auth->add($createAdmin1);

        // добавляем разрешение "updatePost"
        $createAdmin2 = $auth->createPermission('admin2');
        $createAdmin2->description = 'Admin2';
        $auth->add($createAdmin2);

        $createSeller1 = $auth->createPermission('seller1');
        $createSeller1->description = 'Seller1';
        $auth->add($createSeller1);

        $createSeller2 = $auth->createPermission('seller2');
        $createSeller2->description = 'Seller2';
        $auth->add($createSeller2);
        // добавляем роль "author" и даём роли разрешение "createPost"
        $seller = $auth->createRole('seller');
        $auth->add($seller);
        $auth->addChild($seller, $createSeller1);
        $auth->addChild($seller, $createSeller2);

        // добавляем роль "admin" и даём роли разрешение "updatePost"
        // а также все разрешения роли "author"
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $createAdmin1);
        $auth->addChild($admin, $createAdmin2);

        // Назначение ролей пользователям. 1 и 2 это IDs возвращаемые IdentityInterface::getId()
        // обычно реализуемый в модели User.
    }
}