<?php

use yii\db\Migration;

class m170908_075828_add_column_user_role extends Migration
{
    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('user', 'role', $this->string(255)->notNull());
    }

    public function down()
    {
        $this->dropColumn('user', 'role');
    }
    
}
