<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\components\Calculator;


$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;

$provider = $model->search();
?>

<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= yii\grid\GridView::widget([
        'dataProvider' => $provider,
        'columns' => [
            [
                'label' => 'id', 
                'attribute' => 'id'
            ],
            [
                'label'     => 'user', 
                'format'    => 'raw',
                'value'     => function($order){
                    return $order['user']->username;                     
                }
            ],
            [
                'label'     => 'Order detail', 
                'format'    => 'raw',
                'value'     => function($order){
                    
                    $mess = '';
                    foreach ($order['items'] as $key => $item) {
                        $mess .= 'Product : '.$item->product_name.' '.Html::img('@upload/'.$item->image, ['style' => ['width' => '100px', 'height' => '100px']]) .' count : '. $order['orderDetails'][$key]['count'].' price '.$item->price.'<br>';
                    }
                    return $mess;                     
                }
            ],
            [
                'label'     => 'total', 
                'format'    => 'raw',
                'value'     => function($order){                  
                    
                    return $order->totalPrice;                     
                }
            ],
            [
                'label'     => 'status', 
                'format'    => 'raw',
                'value'     => function($order){                  
                    
                    return $order->status;                     
                }
            ],

        ]
    ]);

    ?>
    <div class="row">
        <div class="col-lg-5">

        </div>
    </div>
</div>