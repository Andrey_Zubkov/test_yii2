<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\models\Product;
use yii\data\ActiveDataProvider;

$query = Product::find();

$provider = new ActiveDataProvider([
    'query' => $query,
    'pagination' => [
        'pageSize' => 3,
    ],
]);

$this->title = 'All products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">


         <?= yii\grid\GridView::widget([
        'dataProvider' => $provider,
        'columns' => [
            [
                'label' => 'id', 
                'attribute' => 'id'
            ],
            [
                'label'     => 'Product name', 
                'format'    => 'raw',
                'value'     => function($product){
                    return $product->product_name;                     
                }
            ],
            [
                'label'     => 'Price', 
                'format'    => 'raw',
                'value'     => function($product){                    
                    
                    return $product->price;                     
                }
            ],
            [
                'label'     => 'Quantity', 
                'format'    => 'raw',
                'value'     => function($product){                    
                    
                    return $product->quantity;                     
                }
            ],
            [
                'label'     => 'Product image',
                'format'    => 'raw',
                'value'     => function($product){
                    return Html::img('@upload/'.$product->image, ['style' => ['width' => '100px', 'height' => '100px']]);
                }
            ],
            [
                'label'     => '', 
                'format'    => 'raw',
                'value'     => function($product){                  
                    
                    return Html::a('Add to cart', ['/cart/add-item', 'id' => $product->id], ['class' => 'profile-link']);
                }
            ],
            [
                'label'     => '', 
                'format'    => 'raw',
                'value'     => function($product){                  
                    
                    return Html::a('Edit', ['product/edit-product', 'id' => $product->id], ['class' => 'profile-link']);
                }
            ],
        ]
    ]);

    ?>
        </div>
    </div>
</div>




                

            