<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\models\Product;

$this->title = $title;
$model = \Yii::createObject(['class' => Product::className()]);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill new product:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>

                <?= $form->field($model, 'product_name')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'price') ?>

                <?= $form->field($model, 'quantity') ?>

                <?= $form->field($model, 'image')->fileInput() ?>

                <div class="form-group">
                    <?= Html::submitButton($but_name, ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>