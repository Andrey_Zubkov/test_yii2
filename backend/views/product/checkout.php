<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\models\Cart;

$cart = Cart::get_cart();
$this->title = 'Checkout';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>


    <div class="row">
        <div class="col-lg-5">
        <?php if(isset($mess)): ?>
            <p><?=$mess?></p>
        <?php endif;?>
        <?php if(!empty($cart)): ?>
            <?php $form = ActiveForm::begin(['id' => 'all_product']); ?>
            <?php foreach ($cart as $key => $item):?>
                <div style="border: 2px solid black;">
                <p>Product name: <?=$item['product_name']?></p>
                <p>Product price: <?=$item['price']?></p>
                <p>Quantity: <?=$item['quantity']?></p>               
                </div>
            <?php endforeach; ?>
            <?= Html::a('Order', ['/cart/insert-order'], ['class' => 'profile-link']) ?><br>
            <?php ActiveForm::end(); ?>
        <?php endif; ?>
        </div>
    </div>
</div>