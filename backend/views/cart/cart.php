<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\models\Cart;

$cart = Cart::get_cart();

$this->title = 'Cart';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>


    <div class="row">
        <div class="col-lg-5">
        <?php if(!empty($cart)): ?>
        	<?php $form = ActiveForm::begin(['id' => 'all_product']); ?>
        	<?php foreach ($cart as $key => $item):?>
        		<div style="border: 2px solid black;">
        		<p>Product name: <?=$item['product_name']?></p>
        		<p>Product price: <?=$item['price']?></p>
        		<p>Quantity: <?=$item['quantity']?></p>
                <?= Html::a('Delete', ['/cart/update', 'product_name' => $item['product_name']], ['class' => 'profile-link']) ?><br> 
        		</div>
        	<?php endforeach; ?>
        	<?= Html::a('Checkout', ['/product/checkout'], ['class' => 'profile-link']) ?><br> 
        	<?php ActiveForm::end(); ?>
        <?php else: ?>
        	<p>Your cart is empty!</p>
        <?php endif; ?>
        </div>
    </div>
</div>