<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->params['breadcrumbs'][] = 'Calculator';
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'calculator']); ?>

            <?= $form->field($model, 'property_price') ?>

            <?= $form->field($model, 'deposit') ?>

            <?= $form->field($model, 'annual_interest') ?>

            <?= $form->field($model, 'year') ?>

            <div class="form-group">
                <?= Html::submitButton('Calculate', ['class' => 'btn btn-primary', 'name' => 'calculate']) ?>
            </div>

            <?php ActiveForm::end(); ?>
            <?php if(isset($result) && !empty($result)): ?>
                <p>Price : <?=$result['price_percentage']?>%</p>
                <p>Interest : <?=$result['interest_percentage']?>%</p>
                <p>Deposit : <?=$result['deposit_percentage']?>%</p>
                <p>$<?=$result['price_month']?> / MONTH</p>
            <?php endif;?>
        </div>
    </div>
</div>