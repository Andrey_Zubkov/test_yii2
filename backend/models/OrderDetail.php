<?php
namespace backend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use backend\models\Cart;
use backend\models\Product;
use backend\models\Order;
/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class OrderDetail extends ActiveRecord
{
	public function rules()
    {
        return [
            [['product_id','order_id','count'], 'required'],
       
        ];
    }

     public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            
           

            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {   
        parent::afterSave($insert, $changedAttributes);

    }

}