<?php
namespace backend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use common\models\User;
use backend\components\Send;
/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class Order extends ActiveRecord
{

    public function rules()
    {
        return [
            [['user_id'], 'required'],
            ['status', 'default', 'value' => 'waiting'],
        
        ];
    }

     public function behaviors()
     {
         return [
                 [
                     'class' => Send::className(),
                 ],
         ];
     }

    public function getItems()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])
            ->viaTable('order_detail', ['order_id' => 'id']);
    }

    public function getOrderDetails()
    {
        return $this->hasMany(OrderDetail::className(), ['order_id' => 'id'])->alias('od');
    }

    public function getTotalPrice()
    {
        return $this->getOrderDetails()->joinWith('product')->groupBy('od.order_id')->select(['total_price' => 'sum(od.count*product.price)'])->scalar();
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {   

        parent::afterSave($insert, $changedAttributes);

        return true; 

    }

}