<?php
namespace backend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\base\Model;
use backend\models\Product;
/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class Cart extends Model
{
	
    public static function add_to_cart($id)
    {
        $cookies = Yii::$app->request->cookies;
        $model_product = new Product();
        $product = $model_product->check_product($id);
        if($cookies->getValue('cart') == NULL || empty($cookies->getValue('cart')))
        {
            $cookies = Yii::$app->response->cookies;
            $new_product_to_cart = array();
            $new_product_to_cart[0]['id'] = $product->id;
            $new_product_to_cart[0]['product_name'] = $product->product_name;
            $new_product_to_cart[0]['price'] = $product->price;
            $new_product_to_cart[0]['quantity'] = 1;
            $cookies->add(new \yii\web\Cookie([
                'name' => 'cart',
                'value' => $new_product_to_cart,
            ]));
            return true;
        } else {            
            $new_product_to_cart = array();
            $old_cart = $cookies->getValue('cart');
            $check_product = true;            
            foreach ($old_cart as $key => $item) {
                if($item['product_name'] == $product->product_name)
                {
                    $new_product_to_cart[$key]['id'] = $item['id'];
                    $new_product_to_cart[$key]['product_name'] = $item['product_name'];
                    $new_product_to_cart[$key]['price'] = $item['price'];
                    $new_product_to_cart[$key]['quantity'] = $item['quantity'] + 1;
                    $check_product = false;

                } else {
                    $new_product_to_cart[$key]['id'] = $item['id'];
                    $new_product_to_cart[$key]['product_name'] = $item['product_name'];
                    $new_product_to_cart[$key]['price'] = $item['price'];
                    $new_product_to_cart[$key]['quantity'] = $item['quantity']; 
                }                               
            }
            if($check_product)
            {
                 $i = count($new_product_to_cart);
                 $new_product_to_cart[$i]['id'] = $product->id;
                $new_product_to_cart[$i]['product_name'] = $product->product_name;
                $new_product_to_cart[$i]['price'] = $product->price;
                $new_product_to_cart[$i]['quantity'] = 1;
            }
                       
            $cookies = Yii::$app->response->cookies;
            $cookies->add(new \yii\web\Cookie([
                'name' => 'cart',
                'value' => $new_product_to_cart,
            ]));
            return true;
        }
        

        return false;
    }

    public static function get_cart()
    {
        $cookies = Yii::$app->request->cookies;
        return $cookies->getValue('cart');
    }

    public static function update_cart($product_name)
    {
        $cookies = Yii::$app->request->cookies;
        $new_product_to_cart = array();
        $old_cart = $cookies->getValue('cart');
        $count = 0;
        foreach ($old_cart as $key => $item) {
            if($item['product_name'] != $product_name)
            {
                $new_product_to_cart[$count]['product_name'] = $item['product_name'];
                $new_product_to_cart[$count]['price'] = $item['price'];
                $new_product_to_cart[$count]['quantity'] = $item['quantity'];
                $count++;
            }
        }

        $cookies = Yii::$app->response->cookies;
            $cookies->add(new \yii\web\Cookie([
                'name' => 'cart',
                'value' => $new_product_to_cart,
            ]));

            return true;

    }

    public static function get_info_cart()
    {
        $cookies = Yii::$app->request->cookies;
        $cart = $cookies->getValue('cart');
        $description = '';
        foreach ($cart as $key => $item) {
            $description .= $item['product_name'].' '.$item['quantity'].', '; 
        }

        return $description;

    }

    public static function clear_cart()
    {
        $cookies = Yii::$app->response->cookies;
        $cookies->remove('cart');

        return true;
    }

    public static function get_items_cart()
    {
        $products = \Yii::createObject(['class' => Product::className()]);

        $cart = Cart::get_cart();

        foreach ($cart as $key => $item) {
            $product = $products->findOne($item['id']);
            $product->quantity -= $item['quantity'];
            $product->save();
            $items[$key]['product'] = $product;
            $items[$key]['count'] = ['count' => $item['quantity']];
        }
        return $items;
    }
}