<?php
namespace backend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use backend\models\Order;
/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class Product extends ActiveRecord
{
    const SCENARIO_ADD = 'add';
    const SCENARIO_EDIT = 'edit';

	public function rules()
    {
        return [
            [['product_name','price','quantity'], 'required'],
            [['created_at','updated_at'],'trim'],
            [['quantity', 'price'], 'integer'],
            [['image'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'on' => self::SCENARIO_ADD],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }

    public function check_product($id)
    {
    	$product = $this->findOne($id);
    	if($product->quantity != 0)
    	{
    		return $product; 
    	} else {
    		return false;
    	}
    }

    public function upload()
    {
        if ($this->validate()) {
             $this->image->saveAs(__DIR__.'/../../uploads/' . $this->image->baseName . '.' . $this->image->extension);
            return true;
        } else {
            return false;
        }
    }

}