<?php
namespace backend\models;
use common\models\base\BaseSearch;
use backend\models\Order;

class OrderSearch extends BaseSearch
{

    public $query;

    public function rules()
    {
        return [
            //query rules
            ['query', 'required'],
            ['query', 'trim'],
            ['query', 'string', 'max' => 255],

        ];
    }

    /**
     * @return $this
     */
    public function buildQuery()
    {
        $query = Order::find()
            ->with('orderDetails')
            ->with('user');

        return $query;
    }

    /**
     * Sort rules
     * @return array
     */
    public function getSort()
    {
        return [
            'defaultOrder' => [
                'id' => 'DESC',
            ],
        ];
    }
}