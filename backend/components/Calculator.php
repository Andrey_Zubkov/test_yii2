<?php
namespace backend\components;

use yii\base\Model;

class Calculator extends Model
{
    /*
     * Range year
     * @var integer
     */
    CONST RANGE_YEAR = ['min' => 2, 'max' => 10];

    /*
     * Range property price
     * @var integer
     */
    CONST RANGE_PROPERTY_PRICE = ['min' => 0, 'max' => 21500];

    /*
     * Range annual interest
     * @var integer
     */
    CONST RANGE_ANNUAL_INTEREST = ['min' => 0, 'max' => 100];

    /*
     * Range deposit
     * @var integer
     */
    CONST RANGE_DEPOSIT = ['min' => 0, 'max' => 21500];

    /*
     * Property price
     * @var integer
     */
    public $property_price = 21500;

    /*
     * Price percentage
     * @var integer
     */
    public $price_percentage;

    /*
     * Annual interest
     * @var integer
     */
    public $annual_interest = 10;

    /*
     * Annual interest percentage
     * @var integer
     */
    public $interest_percentage;

    /*
     * Annual interest number
     * @var integer
     */
    public $annual_interest_number;

    /*
     * Deposit
     * @var integer
     */
    public $deposit = 10750;

    /*
     * Deposit percentage
     * @var integer
     */
    public $deposit_percentage;

    /*
     * Year
     * @var integer
     */
    public  $year = 2;

    /*
     * Price / month
     * @var integer
     */
    public $price_month;

    /*
     * Total price
     * @var integer
     */
    public $total;



    public function attributeLabels()
    {
        return [
            'property_price' => 'Property price:',
            'deposit' => 'Deposit:',
            'annual_interest' => 'Annual interest:',
            'year' => 'Year:',
        ];
    }

    public function rules()
    {
        return [
            [['property_price', 'deposit', 'annual_interest', 'year'], 'required'],

            ['annual_interest', 'integer', 'min' => self::RANGE_ANNUAL_INTEREST['min'], 'max' => self::RANGE_ANNUAL_INTEREST['max']],
            ['annual_interest', 'default', 'value' => 0],

            ['year', 'number', 'min' => self::RANGE_YEAR['min'], 'max' => self::RANGE_YEAR['max'],],
            ['year', 'default', 'value' => 2],

            ['deposit', 'integer', 'min' => self::RANGE_DEPOSIT['min'], 'max' => self::RANGE_DEPOSIT['max']],
            ['deposit', 'default', 'value' => 10750],

            ['property_price', 'integer', 'min' => self::RANGE_PROPERTY_PRICE['min'], 'max' => self::RANGE_PROPERTY_PRICE['max']],
            ['property_price', 'default', 'value' => 21500],
        ];
    }

    /**
     * Get price / month
     * @return float
     */
    private function mortgagePayment($p, $r, $y)
    {
        return $this->futureValue($p, $r, $y) / $this->geomSeries(1 + $r, 0, $y - 1);
    }

    private function futureValue($p, $r, $y)
    {
        return $p * pow(1 + $r, $y);
    }

    private function geomSeries($z, $m, $n)
    {
        if($z == 1.0)
        {
            $amt = $n + 1;
        } else {
            $amt = (pow($z , $n + 1) - 1) / ($z - 1);
        }
        if($m >= 1)
        {
            $amt -= $this->geomSeries($z, 0, $m - 1);
        }

        return $amt;
    }

    /**
     * Get result calculate
     */
    public function calculate()
    {
        $price = $this->property_price - $this->deposit;
        $annual = $this->annual_interest / 100;
        $this->price_month = round($this->mortgagePayment($price, $annual / 12, $this->year * 12),2);
        if($this->price_month > 0)
        {
            $this->annual_interest_number = ($price * $this->year) / 100;
            $this->total = $price + $this->deposit + $this->annual_interest_number;
            $this->price_percentage = round(($price / $this->total) * 100);
            $this->deposit_percentage = round(($this->deposit / $this->total) * 100);
            $this->interest_percentage = round(($this->annual_interest_number / $this->total) * 100);
        }

    }

}