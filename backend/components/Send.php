<?php

namespace backend\components;
use Yii;
use yii\db\ActiveRecord;
use yii\base\Behavior;
use backend\models\Cart;
use backend\models\Order;
use backend\models\Product;
class Send extends Behavior
{
    public $model;

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSaveOrder',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSaveOrder'
        ];
    }
    public function beforeSaveOrder($event)
    {
        if(!isset($this->owner->status) || empty($this->owner->status))
        {
            $this->owner->status = 'waiting';
        }

        $items = Cart::get_items_cart();

        $this->owner->populateRelation('items', $items);

        return true;
    }
    public function afterSaveOrder($event)
    {
        $relatedRecords = $this->owner->getRelatedRecords();
        if (isset($relatedRecords['items'])) {
            foreach ($relatedRecords['items'] as $key => $item) {
                $this->owner->link('items', $item['product'], $item['count']);
            }
        }
        Cart::clear_cart();
        return true;
    }
}