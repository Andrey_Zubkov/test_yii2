<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Product;
use backend\models\Cart;
use backend\models\Order;
/**
 * Site controller
 */
class CartController extends Controller
{
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['add-item', 'cart', 'update','insert-order'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],

        ];
    }

    public function actionAddItem($id)
    {        
        if(Cart::add_to_cart($id))
        {
            Yii::$app->session->setFlash('success', "Product successfuly added to cart!");
        } else {
            Yii::$app->session->setFlash('fail', "Product not successfuly added to cart!");
        }
        return $this->redirect(['/product/all-product/']);
    }

    public function actionCart()
    {
        return $this->render('cart');               
        
    }
    public function actionUpdate($product_name)
    {
        if(Cart::update_cart($product_name))
        {
            Yii::$app->session->setFlash('success', "Product successfuly delete!");
        } else {
            Yii::$app->session->setFlash('fail', "Product not successfuly delete!");
        }
        $this->redirect(['cart']);
    }

    public function actionInsertOrder()
    {

        $model = \Yii::createObject(['class' => Order::className()]);

        $user = \Yii::$app->user->identity;

        if(!empty($user))
        {
            $user->link('orders',$model);
            Yii::$app->session->setFlash('success', "Order successfuly insert!");
        } else {
            Yii::$app->session->setFlash('fail', "Order not successfuly insert!");
        }

        $this->redirect(['/product/checkout/']);
        
    }
}