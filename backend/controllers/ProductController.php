<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Product;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use backend\models\OrderSearch;
use backend\models\Cart;
use backend\models\Order;
/**
 * Site controller
 */
class ProductController extends Controller
{
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['add-product','all-product', 'edit-product','cart', 'checkout','orders'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],

        ];
    }

    public function actionAddProduct()
    {
    	$model = \Yii::createObject(['class' => Product::className()]);

        $model->load(Yii::$app->request->post());

    	if($model->load(Yii::$app->request->post()))
    	{
            $model->image = UploadedFile::getInstance($model, 'image');
            if ($model->save()) {
                if($model->upload())
                {
                    Yii::$app->session->setFlash('success', "Product successfuly added!");
                } else {
                    Yii::$app->session->setFlash('fail', "Product not added!");
                }
            }
    		
    	} 

    	return $this->render('addproduct',['title' => 'Add product', 'but_name' => 'Add']);
    	
    	
    }

    public function actionAllProduct()
    {
        return $this->render('allproduct');    	
    }

    public function actionEditProduct($id)
    {	
    	$model = Product::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    	if($model->load(Yii::$app->request->post()) && $model->validate())
    	{
    		if($model->save())
    		{
    			Yii::$app->session->setFlash('success', "Product successfuly update!");
    		} else {
    			Yii::$app->session->setFlash('fail', "Product not update!");
    		}
    	} 
    	
        return $this->render('editproduct', ['title' => 'Edit product', 'but_name' => 'Update', 'id' => $id]);
    	
    	
    }


    public function actionCheckout()
    {
        return $this->render('checkout');    	
    }

    public function actionOrders()
    {
        $model = \Yii::createObject(['class' => OrderSearch::className()]);

    	return $this->render('orders', ['model' => $model]);
    }
}