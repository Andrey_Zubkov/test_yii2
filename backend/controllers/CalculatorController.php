<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use backend\components\Calculator;

/**
 * Site controller
 */
class CalculatorController extends Controller
{
    public function actionIndex()
    {   $model = \Yii::createObject(['class' => Calculator::className()]);

        if($model->load(Yii::$app->request->post()) && $model->validate())
        {
            $result = $model->calculate();

            return $this->render('index', ['model' => $model, 'result' => $result]);
        }
        return $this->render('index', ['model' => $model]);
    }
}